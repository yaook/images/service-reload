#!/bin/sh
##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
set -eu
echo "Triggering reload of mysql now."
if test -n "${MARIADB_ROOT_USER:-}" && test -n "${MARIADB_ROOT_PASSWORD}" && mysql -u"$MARIADB_ROOT_USER" -p"$MARIADB_ROOT_PASSWORD" --socket "$MARIADB_SOCKET" -e 'FLUSH SSL;'; then
    printf 'Successfully reloaded certificates via SQL\n'
    exit 0
fi
printf 'Failed to run flush-ssl via SQL, will resort to stopping mariadb\n'

host_index=$(hostname | grep -o '[[:digit:]][[:digit:]]*$' || echo '0')
delay=$((host_index * 300 + RANDOM / 1000))
printf 'Will kill mysqld after %d seconds (host_index=%d)\n' "$delay" "$host_index"
sleep "$delay"
printf 'Delay is over, killing now!\n'
kill -TERM $(pidof mysqld)
